[TOC]

# 1. Introduzione #

La seguente documentazione tecnica è inerente al progetto denominato **Crudemo**.

Per scaricare il progetto, effettuare il clone del repository con il seguente comando: 

```shell
git clone https://AsoStrifeSpindox@bitbucket.org/AsoStrifeSpindox/crudemo.git
```

L'applicazione offre una serie di API per svolgere operazioni CRUD sull'utenza salvata all'interno di un database `MySql` ed uno `MongoDB`.

# 2. Requisiti & Dipendenze

Se si vuole ricreare il progetto da zero, partendo da un template [Spring Initializr](https://start.spring.io/) assicurarsi di selezionare le seguenti dipendenze e requisiti: 

## 2.1 Requisiti

- Java 8
- Maven
- Springboot 2.5.5

## 2.2 Dipendenze 

- spring-boot-starter-data-jpa
- spring-boot-starter-data-mongodb
- spring-boot-starter-data-rest
- spring-boot-starter-web
- h2
- mysql-connector-java
- lombok 1.18.8

## 2.3 Database 

L'applicazione necessità dei seguenti DB per poter funzionare. Seguire le istruzioni nella documentazione ufficiale per l'installazione. 

-  [MySQL](https://www.mysql.com/it/)
- [MongoDB](https://www.mongodb.com/it-it)

# 3. Configurazione

All'interno del file `application.properties`  configurare le credenziali di accesso ai database `MySql` e `MongoDB`. 

```ini
#spring.jpa.hibernate.ddl-auto=create-drop
spring.jpa.properties.hibernate.globally_quoted_identifiers=true
spring.mvc.throw-exception-if-no-handler-found=true
spring.web.resources.add-mappings=false

# mysql
spring.datasource.url=jdbc:mysql://localhost/crudemo
spring.datasource.username=root
spring.datasource.password=
spring.datasource.driverClassName=com.mysql.jdbc.Driver

# MongoDB
spring.data.mongodb.authentication-database=admin
spring.data.mongodb.database=crudemo
spring.data.mongodb.port=27017
spring.data.mongodb.host=localhost
spring.main.allow-bean-definition-overriding=true
```

In alternativa si può configurare il file `application.yml`

```yaml
spring:
  jpa:
    properties:
      hibernate:
        globally_quoted_identifiers: true
  datasource:
    url: jdbc:mysql://localhost/crudemo
    username: root
    password:
    driverClassName: com.mysql.jdbc.Driver
  data:
    mongodb:
      authentication-database: admin
      database: crudemo
      port: 27017
      host: localhost
```

---

Di seguito si può vedere il `Pom.xml` per gestire le dipendenze. 

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<parent>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-parent</artifactId>
		<version>2.5.4</version>
		<relativePath/> <!-- lookup parent from repository -->
	</parent>
	<groupId>com.spindox</groupId>
	<artifactId>crudemo</artifactId>
	<version>0.0.1-SNAPSHOT</version>
	<name>crudemo</name>
	<description>Demo project for Spring Boot</description>
	<properties>
		<java.version>1.8</java.version>
	</properties>
	<dependencies>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-data-jpa</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-data-mongodb</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-data-rest</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
		</dependency>

		<dependency>
			<groupId>com.h2database</groupId>
			<artifactId>h2</artifactId>
			<scope>runtime</scope>
		</dependency>
		<dependency>
			<groupId>mysql</groupId>
			<artifactId>mysql-connector-java</artifactId>
			<scope>runtime</scope>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-test</artifactId>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>io.projectreactor</groupId>
			<artifactId>reactor-test</artifactId>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.projectlombok</groupId>
			<artifactId>lombok</artifactId>
			<version>1.18.8</version>
		</dependency>
		<dependency>
			<groupId>org.projectlombok</groupId>
			<artifactId>lombok</artifactId>
		</dependency>
	</dependencies>

	<build>
		<plugins>
			<plugin>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-maven-plugin</artifactId>
			</plugin>
		</plugins>
		<resources>
			<resource>
				<directory>src/main/resources</directory>
				<filtering>true</filtering>
			</resource>
		</resources>
	</build>


</project>

```

I log vengono gestiti tramite la libreria `logback.xml`. E' possibile configurare la `path ` dei log all'interno del tag: `<property name="LOGS" value="PATH/DEL/FILE" />`. Il formato dei log è indicato all'interno del tag `<Pattern></Pattern>`. I file di log vengono creati seguendo la nomenclatura specificata nel tag `<fileNamePattern></fileNamePattern>` ovvero per ogni giorno ci sarà un file `.log` così denominato: `log-%d{yyyy-MM-dd}.%i.log`. Di seguito la configurazione:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<configuration>

    <property name="LOGS" value="C:/Users/Utente/Desktop/Crudemolog/" />

    <appender name="Console"
              class="ch.qos.logback.core.ConsoleAppender">
        <layout class="ch.qos.logback.classic.PatternLayout">
            <Pattern>
                %black(%d{ISO8601}) %highlight(%-5level) [%blue(%t)] %yellow(%C{1.}): %msg%n%throwable
            </Pattern>
        </layout>
    </appender>


    <appender name="RollingFile" class="ch.qos.logback.core.rolling.RollingFileAppender">
        <encoder
                class="ch.qos.logback.classic.encoder.PatternLayoutEncoder">
            <Pattern>%d %p %C{1.} [%t] %m%n</Pattern>
        </encoder>

        <rollingPolicy
                class="ch.qos.logback.core.rolling.TimeBasedRollingPolicy">
            <!-- rollover daily and when the file reaches 10 MegaBytes -->
            <fileNamePattern>${LOGS}/log-%d{yyyy-MM-dd}.%i.log
            </fileNamePattern>
            <timeBasedFileNamingAndTriggeringPolicy
                    class="ch.qos.logback.core.rolling.SizeAndTimeBasedFNATP">
                <maxFileSize>10MB</maxFileSize>
            </timeBasedFileNamingAndTriggeringPolicy>
        </rollingPolicy>

        <!-- Nel file salvo solo dal livello ERROR in su -->
        <filter class="ch.qos.logback.classic.filter.ThresholdFilter">
            <level>ERROR</level>
        </filter>
    </appender>

    <!-- LOG everything at INFO level -->
    <root level="info">
        <appender-ref ref="RollingFile" />
        <appender-ref ref="Console" />
    </root>

    <!-- LOG "com.baeldung*" at TRACE level -->
    <logger name="com.spindox.crudemo" level="trace" additivity="false">
        <appender-ref ref="RollingFile" />
        <appender-ref ref="Console" />
    </logger>


</configuration>
```



# 3. API

Di default l'applicazione viene eseguita sulla porta `8080`.

## 3.1 Endpoint

### Get User

Restituisce un utente presente nel database `MySql`.

```http
GET /users/{id}
```

| Parametro | Tipo      | Descrizione                                                  |
| --------- | --------- | :----------------------------------------------------------- |
| `id`      | `Integer` | **Required**. ID univoco dell'utente di cui si vuole ottenere le informazioni |

Risposta in caso di successo: 

```json
{
  "status": "OK",
  "message": {
      "id": 1,
      "ipAddress": "0.0.0.0",
      "fullName": "Andrea Corriga",
      "email": "me@andreacorriga.com"
    }
}
```



### Get All Users

Restituisce tutti gli utenti presenti nel database `MySql`.

```http
GET /users
```

Risposta in caso di successo: 

```json
{
  "status": "OK",
  "message": [
    {
      "id": 1,
      "ipAddress": "0.0.0.0",
      "fullName": "Andrea Corriga",
      "email": "me@andreacorriga.com"
    },
    {
      "id": 2,
      "ipAddress": "0.0.0.0",
      "fullName": "Marco Covelli",
      "email": "marco.covelli@spindox.it"
    },
    ...
  ]
}
```



### Get Last User

Restituisce l'ultimo utente inserito nel database `MySql`.

 ```http
GET /users/last
 ```

```
{
  "status": "OK",
  "message": {
      "id": 2,
      "ipAddress": "0.0.0.0",
      "fullName": "Marco Covelli",
      "email": "marco.covelli@spindox.it"
    }
}
```


### Add User

Aggiunge un utente all'interno del database `MySql`.

 ```http
POST /users
 ```

| Parametro  | Tipo     | Descrizione                                         |
| ---------- | -------- | :-------------------------------------------------- |
| `fullName` | `String` | **Required**. Nome completo dell'utente da inserire |
| `email`    | `String` | **Required** Email dell'utente da inserire          |

Risposta in caso di successo:

```json
{
  "status": "CREATED",
  "message": {
    "id": 0,
    "ipAddress": "0.0.0.0",
    "fullName": "Andrea Corriga",
    "email": "me@andreacorriga.com"
  }
}
```



### Update User

Aggiorna un utente.

 ```http
PUT /users/{id}
 ```

| Parametro  | Tipo      | Descrizione                                                  |
| ---------- | --------- | :----------------------------------------------------------- |
| `id`       | `Integer` | **Required**. ID univoco dell'utente di cui si vuole ottenere le informazioni |
| `fullName` | `String`  | **Required**. Nome completo dell'utente da inserire          |
| `email`    | `String`  | **Required** Email dell'utente da inserire                   |

Risposta in caso di successo:

```json
{
  "status": "OK",
  "message": {
    "id": 1,
    "ipAddress": "0.0.0.0",
    "fullName": "Andrea Corriga",
    "email": "me@andreacorriga.com"
  }
}
```



### Delete user

Cancella un utente dal database `MySql`

 ```http
DELETE /users
 ```

| Parametro | Tipo      | Descrizione                                                  |
| --------- | --------- | :----------------------------------------------------------- |
| `id`      | `Integer` | **Required**. ID univoco dell'utente che si vuole cancellare |

Risposta in caso di successo: 

```json
{
  "status": "OK",
  "message": null
}
```



### Save user from MySql DB to MongoDB

Legge un utente dal database `MySql` e lo salva all'interno del database `MongoDB`.

```http
POST /users/mongo/{id}
```

| Parametro | Tipo      | Descrizione                                                  |
| --------- | --------- | :----------------------------------------------------------- |
| `id`      | `Integer` | **Required**. ID univoco dell'utente di cui si vuole ottenere le informazioni |

Risposta in caso di successo: 

```json
{
  "status": "OK",
  "message": {
    "_id": {
      "timestamp": 1632837576,
      "date": "2021-09-28T13:59:36.000+00:00"
    },
    "mysqlID": 1,
    "ipAddress": null,
    "fullName": "Andrea Corriga",
    "email": "me@andreacorriga.com2"
  }
}
```



### Search Users

Cerca un utente all'interno del sistema.

```http
GET /users/search
```

| Parametro | Tipo     | Descrizione                                                  |
| --------- | -------- | :----------------------------------------------------------- |
| `query`   | `String` | **Required**. Query di ricerca. La parola chiave verrà cercata all'interno del campo `email` : `LIKE %query%` |

Risposta con query di ricerca `gmail`:

```json
{
  "status": "OK",
  "message": [
    {
      "id": 1,
      "ipAddress": "5.91.48.53",
      "fullName": "Andrea Corriga",
      "email": "andreacorriga@gmail.com"
    },
    {
      "id": 3,
      "ipAddress": "151.56.54.211",
      "fullName": "Roberto Scalas",
      "email": "robertoscalar@gmail.com"
    },
    ...
  ]
}
```



## 3.2 Status Codes

Le api restituiscono i seguenti status code:

| Status Code | Description             |
| ----------- | ----------------------- |
| 200         | `OK`                    |
| 201         | `CREATED`               |
| 400         | `BAD REQUEST`           |
| 404         | `NOT FOUND`             |
| 500         | `INTERNAL SERVER ERROR` |

# 4. Minishift

## 4.1 Configurazione su Windows

Prima di tutto scaricare i seguenti eseguibili: 

-  `minishift`: [Release v1.34.3 · minishift/minishift (github.com)](https://github.com/minishift/minishift/releases/tag/v1.34.3)
- `oc`: [Release v3.11.0 · openshift/origin (github.com)](https://github.com/openshift/origin/releases/tag/v3.11.0)

E' possibile utilizzare `Hyper-V` oppure `Virtualbox`. In questa documentazione si farà riferimento all'installazione con **Virtualbox**.

Una volta scaricati gli eseguibili copiarli all'interno della cartella `C:\minishift` ed aggiungere questa path alle variabili d'ambiente. E' possibile farlo da interfaccia grafica di Windows10 digitando nel menù start `Modifica le variabili d'ambiente relative al sistema` oppure su `Powershell` digitando: 

```powershell
[Environment]::SetEnvironmentVariable("Path", $env:Path + ";C:\minishift", [EnvironmentVariableTarget]::Machine)
```

Successivamente per installare e configurare `Minishift` con `Virtualbox` digitare i seguenti comandi: 

 ```powershell
 minishift config set vm-driver virtualbox
 minishift start --vm-driver=virtualbox --show-libmachine-logs
 ```

Una volta avviato per aggiungere tutti i `tempaltes` e `imagestream` su `Minishift` digitare il seguente comando:

```powershell
minishift addons apply xpaas
```

Dopo aver avviato `Minishift`, abbiamo bisogno di eseguire alcuni comandi `oc` aggiuntivi per abilitare `source-2-image `per le app Java. Per prima cosa, aggiungiamo alcuni privilegi all'utente admin per essere in grado di accedere a tutti i progetti `OpenShift`. 

```powershell
minishift addons apply admin-user
oc login -u system:admin
oc adm policy add-cluster-role-to-user cluster-admin admin
oc login -u admin -p admin
```

Una volta eseguiti i comandi sarà possibile accedere alla console web utilizzando le credenziali `admin/admin`.

A questo punto da Console Grafica dovremo creare 3 `Application`

- Java | Wildfly, in cui dovrà essere inserito:
  -  Versione `8.1` 
  - Nome applicazione: `crudemo-app`
  - Link al repository: `https://AsoStrifeSpindox@bitbucket.org/AsoStrifeSpindox/crudemo.git`. 
- MySql (inserire le credenziali indicate nel [paragrafo 4.2](#42-mysql))
- MongoDB (inserire le credenziali indicate nel [paragrafo 4.3](#43-mongodb))

Una volta creato e buildato tutto quanto bisognerà esporre il servizio con i seguenti comandi:

```powershell
oc whoami # per essere sicuri di essere loggati come admin
oc crudemo # entriamo nel progetto
oc expose svc crudemo-app
```

Per conoscere la CLI (Command Line Interface) andare all'indirizzo: 

```http
https://192.168.99.100:8443/console/command-line
https://<ip-minishift-locale>:<porta>/console/command-line
```



## 4.2 Mysql

```
namespace: crudemo-mysql
Database Service Name: crudemo-mysql

Username: crudemo
Password: 123465798
Database Name: crudemo
Connection URL: mysql://mysql:3306/
```

## 4.3 MongoDB

```
namespace: crudemo-mongo
Database Service Name: crudemo-mongo

Username: crudemo
Password: 123465789
Database Name: crudemo
Connection URL: mongodb://crudemo:123465789@crudemo-mongo/crudemo
```

## 4.3 Creare progetto e App da console

Qualora si voglia creare il progetto su `Minishift` e l'app `Wildfly` direttamente da terminale eseguire i seguenti comandi da `PoweShell`: 

```powershell
oc new-project crudemo -description="" --display-name="Crudemo"
oc new-app --name=crudemo-app wildfly~https://AsoStrifeSpindox@bitbucket.org/AsoStrifeSpindox/crudemo.git
```



# 5. Test

All'interno del percorso `/src/test/java/com.spindox.crudemo/` sono presenti i seguenti package per i rispettivi test: 

- `com.spindox.crudemo.controller` per effettuare i seguenti test:
  - `UserControllerSmokeTest`
  - `UserControllerTest`
- `com.spindox.crudemo.service` per effettuare i test su:
  -  `UserServiceTest`



# 6. Servizi Esterni

**Crudemo** utilizza `RandomIp` come servizio esterno per generare a runtime un indirizzo ip randomico per la classe `MysqlUser`. 

La documentazione del progetto è disponibile al seguente indirizzo: [AsoStrifeSpindox / randomip — Bitbucket](https://bitbucket.org/AsoStrifeSpindox/randomip/src/master/)



# 7. Link utili

- [Spring Initializr](https://start.spring.io/)
- [Configurare Minishift con Hyper-V o VirtualBox Windows 10](https://www.marksei.com/openshift-minishift-widnows/)
- [How to Run Java Microservices on OpenShift Using Source-2-Image](https://morioh.com/p/b2d34682888b)
- [Scaricare Minishift](https://github.com/minishift/minishift/releases/tag/v1.34.3)
- [Scaricare OC](https://github.com/openshift/origin/releases/latest) 
- [Documentazione CLI OC](https://docs.openshift.com/container-platform/4.2/applications/projects/working-with-projects.html)
- [Comandi CLI Base | CLI Operations | CLI Reference](https://docs.openshift.com/enterprise/3.0/cli_reference/basic_cli_operations.html)
- [JPA Query Creation Method](https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#jpa.query-methods.query-creation)
- [Testing the Web Layer (spring.io)](https://spring.io/guides/gs/testing-web/)
- [Spring boot MockMVC example with @WebMvcTest - HowToDoInJava](https://howtodoinjava.com/spring-boot2/testing/spring-boot-mockmvc-example/)
- [AsoStrifeSpindox / randomip — Bitbucket](https://bitbucket.org/AsoStrifeSpindox/randomip/src/master/)

