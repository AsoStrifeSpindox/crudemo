package com.spindox.crudemo.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@AutoConfigureMockMvc
class UserControllerRealTest {


    @Autowired
    private MockMvc mvc;

    /**
     * Get ok
     * Test for GET /users/{ID} endpoint
     * @throws Exception
     */
    @Test
    void getUserTest() throws Exception{
        this.mvc.perform(get("/users/2/")).andDo(print()).andExpect(status().isOk());
    }

    /**
     * Get All User
     * @throws Exception
     */
    @Test
    void getAllUserTest() throws Exception{
        this.mvc.perform(get("/users/")).andDo(print()).andExpect(status().isOk());
    }

    /**
     * Get Last User
     * @throws Exception
     */
    @Test
    void getLastUserTest()  throws Exception{
        this.mvc.perform(get("/users/last")).andDo(print()).andExpect(status().isOk());
    }

    /**
     * Post Ok
     * Test for POST /users/ endpoint
     * @throws Exception
     */
    @Test
    void addUserTest() throws Exception{
        this.mvc.perform(post("/users/")
                        .param("fullName", "Andrea Corriga")
                        .param("email", "me@andreacorriga.com"))
                        .andDo(print()).andExpect(status().isInternalServerError());
    }


    /**
     * Put Ok
     * Test for PUT /users/{id}
     * @throws Exception
     */
    @Test
    void updateUserTest() throws Exception{
        this.mvc.perform(put("/users/15/")
                        .param("fullName", "Andrea Corriga")
                        .param("email", "me@andreacorriga.com"))
                .andDo(print()).andExpect(status().isInternalServerError());
    }

    /**
     * Delete Ok
     * Test for PUT /users/{id}
     * @throws Exception
     */
    @Test
    void deleteUserTest() throws Exception{
        this.mvc.perform(delete("/users/477/"))
                .andDo(print()).andExpect(status().isBadRequest());
    }

    /**
     * Search User
     * @throws Exception
     */
    @Test
    void searchUserTest() throws Exception{
        this.mvc.perform(get("/users/").param("query", "gmail")).andDo(print()).andExpect(status().isOk());
    }

    /**
     * Mysql to Mongo
     * @throws Exception
     */
    @Test
    void mysqlToMongoTest() throws Exception{
        this.mvc.perform(post("/users/mongo/2/")).andDo(print()).andExpect(status().isOk());
    }
}