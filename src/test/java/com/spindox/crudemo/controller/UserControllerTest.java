package com.spindox.crudemo.controller;

import com.spindox.crudemo.exception.CrudemoException;
import com.spindox.crudemo.model.MysqlUser;
import com.spindox.crudemo.response.ResponseTransfer;
import com.spindox.crudemo.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Optional;

import static org.mockito.AdditionalMatchers.geq;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebMvcTest(UserController.class)
class UserControllerTest {


    @Autowired
    private MockMvc mvc;

    @MockBean
    UserController userController;

    @MockBean
    UserService userService;

    @BeforeEach
    public void mockUserService() throws CrudemoException {
        Optional<MysqlUser> user = Optional.of(new MysqlUser(1, "Andrea", "me@andreacorriga.com"));
        ResponseEntity<ResponseTransfer> responseOk = new ResponseEntity<>(
                new ResponseTransfer(
                        HttpStatus.OK,
                        user
                ),
                HttpStatus.OK
        );

        ResponseEntity<ResponseTransfer> responseBadRequest = new ResponseEntity<>(
                new ResponseTransfer(HttpStatus.BAD_REQUEST), HttpStatus.BAD_REQUEST);

        when(userService.getUser(anyInt())).thenReturn(responseOk);

        when(userService.addUser(anyString(), anyString())).thenReturn(responseOk);

        when(userService.updateUser(anyInt(), anyString(), anyString())).thenReturn(responseOk);

        when(userService.deleteUser(anyInt())).thenReturn(responseOk);

    }

    /**
     * Get ok
     * Test for GET /users/{ID} endpoint
     * @throws Exception
     */
    @Test
    void getUserTest() throws Exception{
        this.mvc.perform(get("/users/1/")).andDo(print()).andExpect(status().isOk());
    }


    /**
     * Post Ok
     * Test for POST /users/ endpoint
     * @throws Exception
     */
    @Test
    void addUserTest() throws Exception{
        this.mvc.perform(post("/users/")
                        .param("fullName", "Andrea Corriga")
                        .param("email", "me@andreacorriga.com"))
                        .andDo(print()).andExpect(status().isOk());
    }


    /**
     * Put Ok
     * Test for PUT /users/{id}
     * @throws Exception
     */
    @Test
    void updateUserTest() throws Exception{
        this.mvc.perform(put("/users/15/")
                        .param("fullName", "Andrea Corriga")
                        .param("email", "me@andreacorriga.com"))
                        .andDo(print()).andExpect(status().isOk());
    }

    /**
     * Delete Ok
     * Test for PUT /users/{id}
     * @throws Exception
     */
    @Test
    void deleteUserTest() throws Exception{
        this.mvc.perform(delete("/users/1/"))
                .andDo(print()).andExpect(status().isOk());
    }


}