package com.spindox.crudemo.service;

import com.spindox.crudemo.exception.CrudemoException;
import com.spindox.crudemo.model.MysqlUser;
import com.spindox.crudemo.model.RandomIp;
import com.spindox.crudemo.repository.MyMongoRepository;
import com.spindox.crudemo.repository.MysqlRepository;
import com.spindox.crudemo.response.ResponseTransfer;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.stubbing.OngoingStubbing;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThatExceptionOfType;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;


@WebMvcTest(UserService.class)
class UserServiceTest {

    @Autowired
    private MockMvc mvc;

    //@Autowired
    //private UserController userController;

    @Autowired
    UserService userService;

    // Mock Repository for offline testing
    @MockBean
    MysqlRepository mysqlRepository;

    @MockBean
    MyMongoRepository myMongoRepository;

    @MockBean
    RestTemplate restTemplate;

    /**
     * Test get user Ok
     * @throws CrudemoException
     */
    @Test
    void testGetUserOk() throws CrudemoException{
        Optional <MysqlUser> user = Optional.of(new MysqlUser(1, "Andrea", "me@andreacorriga.com"));

        when(mysqlRepository.findById(anyInt())).thenReturn(user);
        assertEquals(200, userService.getUser(1).getStatusCodeValue());
    }

    /**
     * Test Exception Invalid ID = 0
     * Test for GET /users/{id}
     *
     * @throws Exception
     */
    @Test
    void testGetUserException(){
        Optional <MysqlUser> user = Optional.of(new MysqlUser(1, "Andrea", "me@andreacorriga.com"));

        when(mysqlRepository.findById(anyInt())).thenReturn(user);
        assertThatExceptionOfType(CrudemoException.class).isThrownBy(() -> userService.getUser(0));
    }

    /**
     * Test Get all user Ok
     * Test for GET /users/
     *
     * @throws Exception
     */
    @Test
    void testGetAllUser() throws CrudemoException{
        List<MysqlUser> users = new ArrayList<MysqlUser>();

        for(Integer i=0; i <= 10; i++){
            MysqlUser user = new MysqlUser(i + 1, "Andrea", "me@andreacorriga.com");
            users.add(user);
        }
        when(mysqlRepository.findAll()).thenReturn(users);
        assertEquals(200, userService.getAllUser().getStatusCodeValue());
    }

    /**
     * Test get last user ok
     * @throws Exception
     */
    @Test
    void testGetLastUser() throws  CrudemoException{
        Optional <MysqlUser> user = Optional.of(new MysqlUser(1, "Andrea", "me@andreacorriga.com"));

        when(mysqlRepository.findTopByOrderByIdDesc()).thenReturn(user);
        assertEquals(200, userService.getLastUser().getStatusCodeValue());
    }


    /**
     * Test add user Ok
     * @throws Exception
     */
    @Test
    void testAddUserOk()  throws  CrudemoException{
        Optional <MysqlUser> user = Optional.of(new MysqlUser(1, "Andrea", "me@andreacorriga.com"));
        RandomIp randomIp = new RandomIp("1.1.1.1", "AsoStrife");

        when(mysqlRepository.save(any())).thenReturn(null);
        when(restTemplate.getForObject(anyString(), any())).thenReturn(randomIp);

        assertEquals(201, userService.addUser("Andrea", "me@andreacorriga.com").getStatusCodeValue());

    }

    /**
     * Test add user Invalid ID
     * @throws Exception
     */
    @Test
    void testAddUserInvalidFields()  throws  CrudemoException{
        Exception exception = assertThrows(CrudemoException.class, () -> {
            userService.addUser("", "");
        });

        assertThatExceptionOfType(CrudemoException.class).isThrownBy(() -> userService.addUser("", ""));
        assertEquals("INVALID_FIELD", exception.getMessage());
    }

    @Test
    void testAddUserIPEmpty() throws  CrudemoException{
        Exception exception = assertThrows(CrudemoException.class, () -> {
            userService.addUser("Andrea Corriga", "me@andreacorriga.com");
        });

        assertThatExceptionOfType(CrudemoException.class).isThrownBy(() -> userService.addUser("Andrea Corriga", "me@andreacorriga.com"));
        assertEquals("INTERNAL_SERVER_ERROR", exception.getMessage());
    }

    /**
     * Test update user Invalid ID
     * Test for PUT /users/{id}
     *
     * @throws Exception
     */
    @Test
    void testUpdateUserInvalidID() throws CrudemoException{
        Optional <MysqlUser> user = Optional.of(new MysqlUser(1, "Andrea", "me@andreacorriga.com"));

        when(mysqlRepository.save(any())).thenReturn(user);

        Exception exception = assertThrows(CrudemoException.class, () -> {
            userService.updateUser(0, "Andrea", "me@andreacorriga.com");
        });

        assertThatExceptionOfType(CrudemoException.class).isThrownBy(() -> userService.updateUser(0, "Andrea", "me@andreacorriga.com"));
        assertEquals("INVALID_ID", exception.getMessage());
    }

    /**
     * Test Update User Invalid Fields
     * Test with email == "" and fullname == ""
     * @throws Exception
     */
    @Test
    void testUpdateUserInvalidFields() throws CrudemoException{
        Optional <MysqlUser> user = Optional.of(new MysqlUser(1, "Andrea", "me@andreacorriga.com"));

        when(mysqlRepository.save(any())).thenReturn(user);

        Exception exception = assertThrows(CrudemoException.class, () -> {
            userService.updateUser(2, "", "");
        });

        assertThatExceptionOfType(CrudemoException.class).isThrownBy(() -> userService.updateUser(2, "", ""));
        assertEquals("INVALID_FIELD", exception.getMessage());
    }

    /**
     * Test Update User Ok
     * @throws CrudemoException
     */
    @Test
    void tesUpdateUserOk() throws CrudemoException{
        MysqlUser user = new MysqlUser(1, "Andrea", "me@andreacorriga.com");
        RandomIp randomIp = new RandomIp("1.1.1.1", "AsoStrife");

        when(mysqlRepository.save(any())).thenReturn(user);
        when(restTemplate.getForObject(anyString(), any())).thenReturn(randomIp);

        assertEquals(200, userService.updateUser(1, "Andrea", "me@andreacorriga.com").getStatusCodeValue());
    }

    /**
     * Test Delete User ok
     * @throws CrudemoException
     */
    @Test
    void tesDeleteUserOk() throws CrudemoException {
        Optional <MysqlUser> user = Optional.of(new MysqlUser(1, "Andrea", "me@andreacorriga.com"));
        assertEquals(200, userService.deleteUser(1).getStatusCodeValue());
    }

    /**
     *
     * @throws CrudemoException
     */
    @Test
    void tesDeleteUserInvalidID() throws CrudemoException {
        Exception exception = assertThrows(CrudemoException.class, () -> {
            userService.deleteUser(0);
        });

        assertThatExceptionOfType(CrudemoException.class).isThrownBy(() -> userService.deleteUser(0));
        assertEquals("INVALID_ID", exception.getMessage());
    }

    /**
     *
     * @throws Exception
     */
    @Test
    void testSearchUserError() throws CrudemoException{
        List<MysqlUser> users = new ArrayList<MysqlUser>();
        for(Integer i=0; i <= 10; i++){
            MysqlUser user = new MysqlUser(i + 1, "Andrea", "me@andreacorriga.com");
            users.add(user);
        }

        when(mysqlRepository.findByEmailContaining(anyString())).thenReturn(users);

        Exception exception = assertThrows(CrudemoException.class, () -> {
            userService.searchUsers("");
        });

        assertThatExceptionOfType(CrudemoException.class).isThrownBy(() -> userService.updateUser(2, "", ""));
        assertEquals("INVALID_FIELD", exception.getMessage());
    }

    /**
     * Test From Mysql to Mongo Ok
     * @throws CrudemoException
     */
    @Test
    void testFromMysqlToMongoOk() throws CrudemoException{
        Optional <MysqlUser> user = Optional.of(new MysqlUser(1, "Andrea", "me@andreacorriga.com"));
        when(mysqlRepository.findById(anyInt())).thenReturn(user);

        assertEquals(200, userService.fromMysqlToMongo(1).getStatusCodeValue());
    }

    /**
     * Test From Mysql to Mongo Invalid ID
     * @throws CrudemoException
     */
    @Test
    void testFromMysqlToMongoInvalidID() throws CrudemoException{
        Exception exception = assertThrows(CrudemoException.class, () -> {
            userService.fromMysqlToMongo(0);
        });

        assertThatExceptionOfType(CrudemoException.class).isThrownBy(() -> userService.fromMysqlToMongo(0));
        assertEquals("INVALID_ID", exception.getMessage());
    }

    /**
     * Test From Mysql to Mongo No User (400)
     * Throw new CrudemoException -> ifPresent() == false
     * @throws CrudemoException
     */
    @Test
    void testFromMysqlToMongoNoUser() throws CrudemoException{
        Exception exception = assertThrows(CrudemoException.class, () -> {
            userService.fromMysqlToMongo(5);
        });

        assertThatExceptionOfType(CrudemoException.class).isThrownBy(() -> userService.fromMysqlToMongo(5));
        assertEquals("INVALID_ID", exception.getMessage());
    }
}