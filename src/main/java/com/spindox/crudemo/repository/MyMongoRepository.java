package com.spindox.crudemo.repository;

import com.spindox.crudemo.model.MongoUser;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MyMongoRepository extends MongoRepository<MongoUser, String> {

}