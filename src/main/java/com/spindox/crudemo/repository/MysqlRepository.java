package com.spindox.crudemo.repository;

import com.spindox.crudemo.model.MysqlUser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

/**
 * Query Methods
 * @url https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#jpa.query-methods.query-creation
 */
public interface MysqlRepository extends JpaRepository<MysqlUser, Integer> {

    Optional<MysqlUser> findTopByOrderByIdDesc();

    Optional<MysqlUser> findFirstByEmail(String email);

    List<MysqlUser> findByEmailContaining(String email);

    List<MysqlUser> findByFullNameContaining(String fullName);

}
