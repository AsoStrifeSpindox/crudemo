package com.spindox.crudemo.exception;

import com.spindox.crudemo.response.ResponseTransfer;
import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import javax.servlet.http.HttpServletRequest;

@Getter
@Setter
public class CrudemoException extends Exception {

    private Integer code;
    private String message;
    private HttpServletRequest request;
    private Logger logger = LoggerFactory.getLogger(CrudemoException.class);

    public static final String INVALID_ID = "INVALID_ID";
    public static final String INVALID_FIELD = "INVALID_FIELD";
    public static final String BAD_REQUEST = "BAD_REQUEST";
    public static final String MISSING_FIELD = "MISSING_FIELD";

    public static final String EMPTY_RESULT_DATA_ACCESS = "EMPTY_RESULT_DATA_ACCESS";
    public static final String NOT_FOUND = "404_NOT_FOUND";
    public static final String USER_NOT_FOUND = "USER_NOT_FOUND";
    public static final String INTERNAL_SERVER_ERROR = "INTERNAL_SERVER_ERROR";
    public static final String NULL_POINTER_EXCEPTION = "NULL_POINTER_EXCEPTION";
    public static final String SQL_INTEGRITY_CONSTRAINT_VIOLATION = "SQL_INTEGRITY_CONSTRAINT_VIOLATION";
    public static final String DATA_INTEGRITY_VIOLATION = "DATA_INTEGRITY_VIOLATION";

    public CrudemoException(Integer code) {
        super();
        this.code = code;
    }
    public CrudemoException(String message, Throwable cause, Integer code) {
        super(message, cause);
        this.code = code;
        this.message = message;
    }

    public CrudemoException(String message, Integer code) {
        super(message);
        this.code = code;
        this.message = message;
    }

    public CrudemoException(Throwable cause, Integer code) {
        super(cause);
        this.code = code;
    }

    /**
     * Handles the exception and saves what happened in the logs
     * with Logback library.
     * @return ResponseEntity<ResponseTransfer>
     * @since 1.0.0
     */
    @ExceptionHandler(CrudemoException.class)
    public final ResponseEntity<ResponseTransfer> handle(){
        HttpStatus httpStatus;
        String m = getMessage();

        switch(this.message){
            case CrudemoException.BAD_REQUEST:
            case CrudemoException.MISSING_FIELD:
            case CrudemoException.INVALID_ID:
            case CrudemoException.EMPTY_RESULT_DATA_ACCESS:
                httpStatus = HttpStatus.BAD_REQUEST;
                break;
            case CrudemoException.NOT_FOUND:
                m = "URI not found: " + request.getRequestURL().toString();
                httpStatus = HttpStatus.NOT_FOUND;
                break;
            case CrudemoException.USER_NOT_FOUND:
                httpStatus = HttpStatus.NO_CONTENT;
                break;
            case CrudemoException.INTERNAL_SERVER_ERROR:
            case CrudemoException.NULL_POINTER_EXCEPTION:
            case CrudemoException.SQL_INTEGRITY_CONSTRAINT_VIOLATION:
            case CrudemoException.DATA_INTEGRITY_VIOLATION:
            default:
                httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
                break;
        }

        logger.error(m);

        return new ResponseEntity<>(
                new ResponseTransfer(
                        httpStatus,
                        m
                ),
                httpStatus
        );
    }



}
