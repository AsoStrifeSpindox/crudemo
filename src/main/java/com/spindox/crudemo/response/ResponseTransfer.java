package com.spindox.crudemo.response;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

@Getter
@Setter
public class ResponseTransfer {
    private Integer status;
    private Object message;

    /**
     *
     */
    public ResponseTransfer(){

    }

    /**
     *
     * @param s
     * @since 1.0.0
     */
    public ResponseTransfer(HttpStatus s){
        this.status = s.value();
    }

    /**
     *
     * @param s
     * @param message
     * @since 1.0.0
     */
    public ResponseTransfer(HttpStatus s, Object message){
        this.status = s.value();
        this.message = message;
    }

}
