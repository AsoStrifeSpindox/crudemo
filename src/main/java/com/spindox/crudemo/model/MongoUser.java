package com.spindox.crudemo.model;

import lombok.Getter;
import lombok.Setter;
import org.bson.types.ObjectId;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class MongoUser {

    @Id
    private ObjectId id;

    private Integer mysqlID;

    private String ipAddress;

    private String fullName;

    private String email;


}