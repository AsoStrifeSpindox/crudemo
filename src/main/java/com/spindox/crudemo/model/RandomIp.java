package com.spindox.crudemo.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RandomIp {

    private String ip;
    private String username;

    public RandomIp(){

    }

    public RandomIp(String ip, String username){
        this.ip = ip;
        this.username = username;
    }
}
