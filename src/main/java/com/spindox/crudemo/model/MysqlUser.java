package com.spindox.crudemo.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name = "users")
public class MysqlUser {


    @Id
    @Column(name = "id")
    private int id;

    @Column(name = "ip_address")
    private String ipAddress;

    @Column(name = "full_name")
    private String fullName;

    @Column(name = "email")
    private String email;

    public MysqlUser(){

    }

    /**
     *
     * @param id
     * @param fullName
     * @param email
     */
    public MysqlUser(Integer id, String fullName, String email){
        this.id = id;
        this.fullName = fullName;
        this.email = email;
    }

    /**
     *
     * @param id
     * @param fullName
     * @param email
     * @param ipAddress
     */
    public MysqlUser(Integer id, String fullName, String email, String ipAddress){
        this.id = id;
        this.fullName = fullName;
        this.email = email;
        this.ipAddress = ipAddress;
    }

    @Override
    public String toString(){
        return this.id + " - " + this.fullName + ", " + this.email + ", " + this.ipAddress;
    }
}