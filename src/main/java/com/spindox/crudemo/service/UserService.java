package com.spindox.crudemo.service;

import com.spindox.crudemo.exception.CrudemoException;
import com.spindox.crudemo.model.MysqlUser;
import com.spindox.crudemo.model.MongoUser;
import com.spindox.crudemo.model.RandomIp;
import com.spindox.crudemo.repository.MyMongoRepository;
import com.spindox.crudemo.repository.MysqlRepository;
import com.spindox.crudemo.response.ResponseTransfer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;

@Component("UseService")
public class UserService {

    @Autowired
    private MysqlRepository mysqlRepository;

    @Autowired
    private MyMongoRepository myMongoRepository;

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Autowired
    private RestTemplate restTemplate;

    Logger logger = LoggerFactory.getLogger(UserService.class);

    /**
     * Given an ID it searches for the user within the Mysql database.
     * The ID cannot be 0.
     * The query returns an Optional<Mysqluser> object because
     * the user associated with that id may not exist.
     * @param id
     * @return ResponseEntity<ResponseTransfer>
     * @throws CrudemoException
     * @since 1.0.0
     */
    public ResponseEntity<ResponseTransfer> getUser(Integer id) throws CrudemoException{
        if(id == 0){
            throw new CrudemoException(CrudemoException.INVALID_ID, 400);
        }

        Optional<MysqlUser> user = mysqlRepository.findById(id);
        if(user.isPresent()){
            return new ResponseEntity<>(
                    new ResponseTransfer(
                        HttpStatus.OK,
                        user
                    ),
                    HttpStatus.OK
            );
        }
        else{
            throw new CrudemoException(CrudemoException.INVALID_ID, 400);
        }
    }

    /**
     *
     * @return ResponseEntity<ResponseTransfer>
     * @throws CrudemoException
     * @since 1.0.0
     */
    public ResponseEntity<ResponseTransfer> getAllUser(){
        return new ResponseEntity<>(
                new ResponseTransfer(
                        HttpStatus.OK,
                        mysqlRepository.findAll()
                ),
                HttpStatus.OK
        );
    }

    /**
     * Returns the last user in the mysql database
     * @return ResponseEntity<ResponseTransfer>
     * @throws CrudemoException
     * @since 1.0.0
     */
    public ResponseEntity<ResponseTransfer> getLastUser() {
        return new ResponseEntity<>(
                new ResponseTransfer(
                        HttpStatus.OK,
                        mysqlRepository.findTopByOrderByIdDesc()
                ),
                HttpStatus.OK
        );
    }

    /**
     * Add a user to the MySql database
     * fullName and email parameters are required
     * @param fullName
     * @param email
     * @return ResponseEntity<ResponseTransfer>
     * @throws CrudemoException
     * @since 1.0.0
     */
    public ResponseEntity<ResponseTransfer> addUser(String fullName, String email) throws CrudemoException  {

        if(fullName.isEmpty() || email.isEmpty()){
            throw new CrudemoException(CrudemoException.INVALID_FIELD, 400);
        }

        MysqlUser user = new MysqlUser();
        user.setFullName(fullName);
        user.setEmail(email);

        RandomIp randomIp = restTemplate.getForObject("http://localhost:8083/ip", RandomIp.class);

        if(randomIp == null) {
            throw new CrudemoException(CrudemoException.INTERNAL_SERVER_ERROR, 500);
        }
        user.setIpAddress(randomIp.getIp());



        mysqlRepository.save(user);

        String logInfo = String.format("L'utente: %s %s salvato con successo.", fullName, email);
        logger.info(logInfo);

        return new ResponseEntity<>(
            new ResponseTransfer(
                    HttpStatus.CREATED,
                    user
            ),
            HttpStatus.CREATED
        );

    }

    /**
     * Updates a user's information
     * fullName and email parameters are required
     * @param id
     * @param fullName
     * @param email
     * @return ResponseEntity<ResponseTransfer>
     * @throws CrudemoException
     * @since 1.0.0
     */
    public ResponseEntity<ResponseTransfer> updateUser(Integer id, String fullName, String email) throws CrudemoException {
        if(id == 0){
            throw new CrudemoException(CrudemoException.INVALID_ID, 400);
        }

        if(fullName.isEmpty() || email.isEmpty()){
            throw new CrudemoException(CrudemoException.INVALID_FIELD, 400);
        }

        MysqlUser user = new MysqlUser();
        user.setId(id);
        user.setFullName(fullName);
        user.setEmail(email);

        RandomIp randomIp = restTemplate.getForObject("http://localhost:8083/ip", RandomIp.class);

        if(randomIp == null) {
            throw new CrudemoException(CrudemoException.INTERNAL_SERVER_ERROR, 500);
        }

        user.setIpAddress(randomIp.getIp());
        MysqlUser userUpdate = mysqlRepository.save(user);

        String logInfo = String.format("Utente aggiornato con ID: %d aggiornato con successo.", id);
        logger.info(logInfo);

        return new ResponseEntity<>(
                new ResponseTransfer(
                        HttpStatus.OK,
                        userUpdate
                ),
                HttpStatus.OK
        );


    }

    /**
     * Delete a user from the database
     * @param id
     * @return ResponseEntity<ResponseTransfer>
     * @throws CrudemoException
     * @since 1.0.0
     */
    public ResponseEntity<ResponseTransfer> deleteUser(Integer id) throws CrudemoException {

        if(id == 0){
            throw new CrudemoException(CrudemoException.INVALID_ID, 400);
        }

        mysqlRepository.deleteById(id);

        String logInfo = String.format("Utente con ID: %d cancellato con successo.", id);
        logger.info(logInfo);

        return new ResponseEntity<>(
                new ResponseTransfer(HttpStatus.OK),
                HttpStatus.OK
        );
    }

    /**
     * Gets a user from the MySql database and saves it in the Mongo database
     * This method executes two queries:
     * a selection from the mysql database and an insert into the mongo database.
     * @param id
     * @return ResponseEntity<ResponseTransfer>
     * @throws CrudemoException
     * @since 1.0.0
     */
    public ResponseEntity<ResponseTransfer> fromMysqlToMongo(Integer id) throws CrudemoException {
        if(id == 0){
            throw new CrudemoException(CrudemoException.INVALID_ID, 400);
        }

        Optional<MysqlUser> mysqlUser = mysqlRepository.findById(id);
        MongoUser mongoUser = new MongoUser();

        if(mysqlUser.isPresent()){
            MysqlUser user = mysqlUser.get();
            mongoUser.setMysqlID(user.getId());
            mongoUser.setFullName(user.getFullName());
            mongoUser.setEmail(user.getEmail());


            myMongoRepository.save(mongoUser);

            String logInfo = String.format("Utente con ID: %d trasferito su DB Mongo con successo.", id);
            logger.info(logInfo);

            return new ResponseEntity<>(
                    new ResponseTransfer(
                            HttpStatus.OK,
                            mongoUser
                    ),
                    HttpStatus.OK
            );

        }
        else{
            throw new CrudemoException("INVALID_ID", 400);
        }


    }

    /**
     * Search for a user in the mysql database with a search query string passed as a parameter.
     * The query would be LIKE %{query_string}%
     * @param email
     * @return ResponseEntity<ResponseTransfer>
     * @throws CrudemoException
     * @since 1.0.0
     */
    public ResponseEntity<ResponseTransfer> searchUsers(String email) throws CrudemoException {
        if(email.isEmpty()){
            throw new CrudemoException("INVALID_FIELD", 400);
        }


        return new ResponseEntity<>(
                new ResponseTransfer(
                        HttpStatus.OK,
                        mysqlRepository.findByEmailContaining(email)
                ),
                HttpStatus.OK
        );

    }


}
