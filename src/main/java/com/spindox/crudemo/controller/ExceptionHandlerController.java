package com.spindox.crudemo.controller;

import com.spindox.crudemo.exception.CrudemoException;
import com.spindox.crudemo.response.ResponseTransfer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.servlet.NoHandlerFoundException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.SQLIntegrityConstraintViolationException;

@ControllerAdvice
public class ExceptionHandlerController {

    CrudemoException e;
    Logger logger = LoggerFactory.getLogger(ExceptionHandlerController.class);

    /**
     * Handles throwable exception
     * Depending on which error has occurred, set the error code and message and handle
     * the error with CrudemoException.handle()
     * @param ex
     * @return ResponseEntity<ResponseTransfer>
     * @since 1.0.0
     */
    @ExceptionHandler(CrudemoException.class)
    public ResponseEntity<ResponseTransfer> handleValidationException(CrudemoException ex) {

        e = new CrudemoException(ex.getMessage(), ex.getCode());
        return e.handle();
    }

    /**
     * Handles runtime exception
     * Depending on which error has occurred, set the error code and message and handle
     * the error with CrudemoException.handle()
     * @param ex
     * @param request
     * @param response
     * @return ResponseEntity<ResponseTransfer>
     * @since 1.0.0
     */
    @ExceptionHandler(Exception.class)
    public ResponseEntity<ResponseTransfer> handle(Exception ex, HttpServletRequest request, HttpServletResponse response) {
        e = new CrudemoException(500);
        e.setRequest(request);

        String errorMessage = ex.toString();
        logger.error(errorMessage);

        if(ex instanceof HttpClientErrorException.BadRequest){
            e.setCode(400);
            e.setMessage("BAD_REQUEST");
        }

        if(ex instanceof MissingServletRequestParameterException){
            e.setCode(400);
            e.setMessage("BAD_REQUEST");
        }

        if(ex instanceof SQLIntegrityConstraintViolationException){
            e.setCode(400);
            e.setMessage("SQL_INTEGRITY_CONSTRAINT_VIOLATION");
        }

        if(ex instanceof DataIntegrityViolationException){
            e.setCode(400);
            e.setMessage("DATA_INTEGRITY_VIOLATION");
        }
        if(ex instanceof EmptyResultDataAccessException){
            e.setCode(400);
            e.setMessage("EMPTY_RESULT_DATA_ACCESS");
        }

        if(ex instanceof  NoHandlerFoundException){
            e.setCode(404);
            e.setMessage("404_NOT_FOUND");
        }

        if (ex instanceof NullPointerException) {
            e.setCode(500);
            e.setMessage("NULL_POINTER_EXCEPTION");
        }

        if(ex instanceof HttpServerErrorException.InternalServerError){
            e.setCode(500);
            e.setMessage("INTERNAL_SERVER_ERROR");
        }


        return e.handle();


    }

}
