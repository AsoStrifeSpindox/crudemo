package com.spindox.crudemo.controller;

import com.spindox.crudemo.exception.CrudemoException;
import com.spindox.crudemo.service.UserService;
import com.spindox.crudemo.response.ResponseTransfer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(path="/users")
public class UserController {

    @Autowired
    private UserService userService;

    /**
     * Get Request. Returns the list of all users
     * @return
     * @throws CrudemoException
     * @since 1.0.0
     * @method GET
     */
    @GetMapping(path="")
    @ResponseBody
    public  ResponseEntity<ResponseTransfer> getAllUsers(){
        return userService.getAllUser();
    }

    /**
     * Post Request.
     * Add a user to the MySql database
     * @param fullName
     * @param email
     * @return
     * @throws CrudemoException
     * @since 1.0.0
     */
    @PostMapping(path="")
    @ResponseBody
    public ResponseEntity<ResponseTransfer> addNewUser (@RequestParam String fullName, @RequestParam String email)  throws CrudemoException {
        return userService.addUser(fullName, email);
    }

    /**
     * Get Request
     * Returns a user's information.
     * The ID parameter is passed in the url
     * like: {base_rul}/users/{id}
     * @param id
     * @return
     * @throws CrudemoException
     * @since 1.0.0
     */
    @GetMapping(path="/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<ResponseTransfer> getUser(@PathVariable("id") Integer id) throws CrudemoException  {
        return userService.getUser(id);
    }

    /**
     * GET Request.
     * Returns the last user in the mysql database
     * @return
     * @throws CrudemoException
     * @since 1.0.0
     */
    @GetMapping(path="/last")
    @ResponseBody
    public  ResponseEntity<ResponseTransfer> getLastUser() {
        return userService.getLastUser();
    }

    /**
     * Put Request
     * Updates a user's information
     * The ID parameter is passed in the url
     * like: {base_rul}/users/{id}
     * @param id
     * @param fullName
     * @param email
     * @return
     * @throws CrudemoException
     * @since 1.0.0
     */
    @PutMapping(path="/{id}")
    @ResponseBody
    public ResponseEntity<ResponseTransfer> updateUser(@PathVariable("id")  Integer id, @RequestParam String fullName, @RequestParam String email) throws CrudemoException{
        return userService.updateUser(id, fullName, email);
    }

    /**
     * Delete Request
     * Delete a user from the database
     * @param id
     * @return
     * @throws CrudemoException
     * @since 1.0.0
     */
    @DeleteMapping(path="/{id}")
    @ResponseBody
    public ResponseEntity<ResponseTransfer> deleteUser(@PathVariable("id") Integer id) throws CrudemoException {
        return userService.deleteUser(id);
    }

    /**
     * Post Request
     * Gets a user from the MySql database and saves it in the Mongo database
     * @param id
     * @return
     * @throws CrudemoException
     * @since 1.0.0
     */
    @PostMapping(path="/mongo/{id}")
    @ResponseBody
    public ResponseEntity<ResponseTransfer> addNewMongoUser (@PathVariable("id") Integer id) throws CrudemoException {
        return userService.fromMysqlToMongo(id);
    }

    /**
     * Get Request
     * Search for a user in the mysql database with a search query string passed as a parameter.
     * The query would be LIKE %{query_string}%
     * @param query
     * @return
     * @throws CrudemoException
     * @since 1.0.0
     */
    @GetMapping(path="/search")
    @ResponseBody
    public ResponseEntity<ResponseTransfer> searchUsers(@RequestParam String query) throws CrudemoException {
        return userService.searchUsers(query);
    }
}